import numpy as np  # python -m pip install numpy
from numpy import linalg as LA
import matplotlib.pyplot as plt   # python -m pip install matplotlib
import math
import random
import sys


"""
Computes the average distribution of eigenvalues for different types of matrices.
"""


def main():
  np.set_printoptions(threshold=sys.maxsize, linewidth=1000)
  checkSemiCircularLaw()


def checkSemiCircularLaw():
  """Plots the distribution of eigenvalues for different kinds of random matrices with different sizes."""
  num_simulations = 100
  # histogram with x going from -3 to 3 by steps of 0.05
  x = list(-3+i*0.05 for i in range(121))
  for n in (5, 20, 100, 1000):
    for generatorName, generator in (
      ('gue', np.random.normal),  # variance 1
      ('uniform', lambda:random.uniform(-math.sqrt(3), math.sqrt(3))),  # variance 1
      ):
      bins = [0] * 121
      for _ in range(num_simulations):
        A = getRandomMatrix(n, generator)
        w, _ = LA.eig(A)
        for eigenvalue in w:
          v = min(max(eigenvalue / math.sqrt(n), -3), 3)
          index_bin = int(20*(v + 3))
          bins[index_bin] += 1
      for i, b in enumerate(bins):
        bins[i] = b / (num_simulations * n)
      plt.hist(x, bins=x, weights=bins)
      plt.title('eigenvalues %s matrix size=%d simulations=%d' % (generatorName, n, num_simulations))
      plt.savefig('./simulations/semicircular_%s_size=%d.png' % (generatorName, n))
      plt.clf()


def getRandomMatrix(n, generator):
  rows = []
  for _ in range(n):
    row = [generator() for _ in range(n)]
    rows.append(row)
  for i in range(len(rows)):
    for j in range(i):
      rows[i][j] = rows[j][i].conjugate()
  return rows


if __name__ == "__main__":
  main()