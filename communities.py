import numpy as np  # python -m pip install numpy
from numpy import linalg as LA
import matplotlib.pyplot as plt   # python -m pip install matplotlib
import math
import random
import sys

"""
This script performs a few checks around random matrices and communities:
- Can we retrieve communities from a random matrix?
- What do eigenvectors look like for the graph expectancy?
- What is the success rate when retrieving 2 communities from the graph with the eigenvector method?
"""

# You can CHANGE these variables
FULL_SIMULATION=False # will run on larger matrices. Slower (but under 5 minutes total)
OUTPUT_LATEX=False  # output suitable for latex

def main():
  np.set_printoptions(threshold=sys.maxsize, linewidth=1000)
  checkCommunities_expectancy()
  checkCommunities_random()
  check_2_communities()


def checkCommunities_expectancy():
  """This tests that communities with different sizes have a maximizing eigenvector
  that is perfectly representative of the communities.
  Recall that the check is on the eigenvectors of the EXPECTANCY matrix"""
  for n in (3, 7, 10, 20, 50, 200):
    for (p, q) in ((.2, .1), (.5, .1), (.1, .01), (.8, .3)):
      A, sizes = communityMatrixExpectancy(n, p, q)
      w, v = LA.eig(A)
      v = np.transpose(v)
      # getting the index of the largest eigenvalue of A and then checking that
      # the corresponding eigenvector has the first n1 values equal, then the n2 values equal etc
      indexMax = max(range(len(w)), key=w.__getitem__)
      maxEigenvector = np.real(v)[indexMax]
      # checking that within the i-th block, all values are the same (up to a rouding error)
      conjectureValid = True
      entriesByBlock = []
      for i, v in enumerate(sizes):        
        s = sum(sizes[:i])
        subArray = maxEigenvector[s:s+sizes[i]]
        if min(subArray) + 1e-5 < max(subArray):  # this checks that all values are the same in the sub array
          conjectureValid = False
        else:
          entriesByBlock.append(min(subArray))
      if conjectureValid:
        nonUniqueWarning = "" if len(set(sizes)) == len(sizes) else "(non unique block sizes)"
        if nonUniqueWarning == "":
          if OUTPUT_LATEX:
            print("%d & %.2f & %.2f & %.4f \\\\" % (n, p, q, min_inter_distance(entriesByBlock)))
          else:
            print("communities expectancy: ok", "n=", n, "p=%.2f, q=%.2f, min_inter_distance=%.4f %s" % (p, q, min_inter_distance(entriesByBlock), nonUniqueWarning))
      else:
        print("communities expectancy: FAIL", "n=", n, "p=%.2f, q=%.2f" % (p, q))


def checkCommunities_random():
  """Prints the largest and 2nd largest eigenvectors associated with a random matrix by block.
  checkCommunities_expectancy() shows that the eigenvectors should be made of blocks of equal values.
  Is it approximately the case here? Can we 'read communities' from the top 2 eigenvectors?"""
  n, p, q = 100, .1, .01
  A, sizes = communityMatrixRandomBlocks(n, p, q)
  w, v = LA.eig(A)
  wl = np.real(w).tolist()
  wSorted = sorted(wl)
  # largest and 2nd largest eigenvectors
  v = np.transpose(v)
  vMax = v[wl.index(wSorted[-1])]
  vMax2 = v[wl.index(wSorted[-2])]
  for eigV in (vMax, vMax2):
    print("eyeballing eigenvector")
    for i, s in enumerate(sizes):
      values = np.real(eigV[sum(sizes[:i]): sum(sizes[:i])+s])
      print("block:%d "%i + ", ".join("%.2f" % val for val in values))


def communityMatrixExpectancy(n, p, q):
  """returns A, sizes where
  * A is a random square matrix of size n (approximately)
  with blocks of ps of sizes n1 != ... != nr and with q elsewhere
  e.g p q q
      q p q
      q q p
  if we picked n1=2 and n2=1
  * sizes is [2, 1] in this example
  """
  sizes = randomBlockSizes(n)
  rows = []
  for i in range(len(sizes)):
    for _ in range(sizes[i]):
      rows.append([0] * sum(sizes[:i]) + [1] * sizes[i] + [0] * sum(sizes[i+1:]))
  K = np.array(rows)
  J = np.array([[1] * n for _ in range(n)])
  return q * J + (p - q) * K, sizes


def check_2_communities():
  """This tests our conjecture that communities with different sizes have a maximizing eigenvector
  that is perfectly representative of the communities."""
  n_simulations = 10
  ns = (20, 50, 200, 1000) if FULL_SIMULATION else (20, 50, 200)
  for normalizePQ in (False, True):  # if True, makes sure n*(p+q) is constant across the different values of n
    print("checking 2 communities " + "with normalization" if normalizePQ else "")
    for (p, q) in ((.01, .001), (.1, .01), (.3, .01), (.3, .05), (.5, .1), (.5, .3)):
      p0, q0 = p, q
      for n in ns:
        p, q = (p0 * 20 / n, q0 * 20 / n) if normalizePQ else (p0, q0)
        sum_wrong = 0
        for _ in range(n_simulations):
          A, sizes = randomMatrix_2_Communities(n, p, q)
          w, v = LA.eig(A)
          ws = sorted(w)
          v = np.transpose(v)
          # getting the index of the 2nd largest eigenvalue of A and then checking that
          # the corresponding eigenvector has the first n1 values of the same sign, then the n2 values equal etc
          index2ndMax = w.tolist().index(ws[-2])
          maxEigenvector2 = v[index2ndMax]
          # Let's say that the eigenvector has entries - - - - + + + - then we can see that the dominating sign
          # is - for the 1st block and + for the 2nd one, and there is just one mistake.
          leftIsPositive = False
          countPositives = sum(1 if x > 0 else 0 for x in maxEigenvector2[:sizes[0]])
          if countPositives > sizes[0] / 2:
            leftIsPositive = True
          countWrongLeft = sum(1 if ((x > 0 and not leftIsPositive) or (x < 0 and leftIsPositive)) else 0 for x in  maxEigenvector2[:sizes[0]])
          countWrongRight = sum(1 if (x > 0 and leftIsPositive) or (x < 0 and not leftIsPositive) else 0 for x in  maxEigenvector2[sizes[0]:])
          sum_wrong += countWrongLeft+countWrongRight 
        if OUTPUT_LATEX:
          if normalizePQ:
            print("%.2f & %d & %.2f & %.3f & %.2f \\\\" % (n*(p+q)/2, n, p, q, (1 - sum_wrong / (n * n_simulations))))
          else:
            print("%.2f & %.3f & %d & %.2f & %.2f \\\\" % (p, q, n, n*(p+q)/2, (1 - sum_wrong / (n * n_simulations))))
        else:
          print("2 communities: n=", n, "n(p+q)/2=", n*(p+q)/2, "p=%.2f, q=%.2f" % (p, q), "success rate: %.2f" % (1 - sum_wrong / (n * n_simulations)))


def randomMatrix_2_Communities(n, p, q):
  """returns A, sizes where
  * A is a random square matrix of size n
  with 0 and 1s with probability p or q depending on their block
  blocks: p q
          q p
  * sizes are the sizes of the blocks
  """

  # Generate random different sub blocks sizes
  # n1, ... nr will be the r first values in 'sizes' so that n1+...+nr >=n
  p = random.uniform(0, 1)
  s1 = min(max(int(n * p), 1), n-1)
  sizes = [s1, n-s1]
  # Generate a matrix with 
  rows = []
  for i in range(2):
    for _ in range(sizes[i]):
      rows.append(
        [binary(q) for _ in range(sum(sizes[:i]))] +
        [binary(p) for _ in range(sizes[i])] +
        [binary(q) for _ in range(sum(sizes[i+1:]))]
      )
  # symmetrize the matrix
  symmetrize(rows)
  return np.array(rows), sizes



def communityMatrixRandomBlocks(n, p, q):
  """Same as communityMatrixExpectancy except that instead of p/q we return a Bernoulli
  random variable(p) (or q)
  """
  sizes = randomBlockSizes(n, unique=True)
  # Generate a matrix of 1s by block with sizes n1, ... nr
  rows = []
  for i in range(len(sizes)):
    for _ in range(sizes[i]):
      rows.append(
        [binary(q) for _ in range(sum(sizes[:i]))] +
        [binary(p) for _ in range(sizes[i])] +
        [binary(q) for _ in range(sum(sizes[i+1:]))]
      )
  symmetrize(rows)
  return np.array(rows), sizes


def randomBlockSizes(n, unique=False):
  """Generates n1, n2, ..., nr so that n1+...+nr = n
  The ni have a law that is centered around in between 1 and n^.7
  The rationale behind this capping is that if we pick blocks that are too big, there are way too few blocks
  """
  sizes = []
  while sum(sizes) < n:
    b = int(math.exp(math.log(n) * random.uniform(0.3, 0.8)))
    b = max(1, min(n-sum(sizes), b))
    if not unique or b not in sizes:
      sizes.append(b)
  return sizes


def symmetrize(rows):
  """Symmetrizes a matrix"""
  for i in range(len(rows)):
    for j in range(i):
      rows[i][j] = rows[j][i].conjugate()


def binary(p):
  ###A random variable = 1 with probability p, 0 otherwise"""
  return random.uniform(0, 1) < p


def min_inter_distance(values):
  """Returns the maximum d such that if i != j, |values[i] - values[j]| >= d"""
  distance = -1
  for i, v1 in enumerate(values):
    for v2 in values[:i]:
      d = v2 - v1 if v2 > v1 else v1 - v2
      if distance < 0 or distance > d:
        distance = d
  return distance


if __name__ == "__main__":
  main()